import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 引了一个语言包（英文）
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n
// import './styles/element-variables.scss'
import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

import Print from 'vue-print-nb'
Vue.use(Print)

import component from '@/components'
Vue.use(component)
// import moment from 'moment'
// Vue.filter('过滤器名字', 过滤器方法)
// Vue.filter('formatterTime', (val, str) => {
//   return moment(val).format(str)
// })

import * as filter from '@/filter'

Object.keys(filter).forEach((item) => {
  Vue.filter(item, filter[item])
})
// import PageTools from '@/components/PageTools'
// import PageTools from '@/components/PageTools'
// import PageTools from '@/components/PageTools'
// import PageTools from '@/components/PageTools'

// Vue.component('PageTools', PageTools)
// Vue.component('PageTools', PageTools)
// Vue.component('PageTools', PageTools)
// Vue.component('PageTools', PageTools)

// import { imgerror, textcolor, xxx, xxx, xxx } from '@/directives'

// Vue.directive('imgerror', imgerror)
// Vue.directive('textcolor', textcolor)
// Vue.directive('xxx', xxx)
// Vue.directive('xxx', xxx)
// Vue.directive('xxx', xxx)

// 优化手段：批量注册  （可以将一个模块中的全部的按需导出，都导入进来）
// * 表示所有模块，* 又是一个关键字，所以 * 要做改名
import * as directives from '@/directives'

// 1. for in 遍历对象，批量注册
// for (const key in directives) {
//   Vue.directive(key, directives[key])
// }

// 场景：假设需要对一个对象里面的key，进行排序sort()
// Object.keys(directives).sort()

// 2. Object.keys(obj) 遍历对象，批量注册
// 工作时遍历对象，会发现：先拿到对象的所有的键，按照工作需求做处理（排序，过滤,...）,最终按照需求遍历
Object.keys(directives).forEach((item) => {
  Vue.directive(item, directives[item])
})

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

import i18n from '@/lang'
// console.log(process.env.VUE_APP_BASE_API)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: (h) => h(App)
})
