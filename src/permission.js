import router from './router'
// import { asyncRoutes } from './router'
import store from './store'
import NProgress from 'nprogress' // 进度条小插件
import 'nprogress/nprogress.css' // progress bar style

// 白名单
const whiteList = ['/login', '/404']
// 路由前置守卫
router.beforeEach(async (to, from, next) => {
  // 开启进度条
  NProgress.start()
  const token = store.getters.token
  if (token) {
    // 有token, 看是否访问的是登录页，如果是登录页，引导到首页
    if (to.path === '/login') {
      next('/')
      NProgress.done()
    } else {
      // 只要正常放行，一定是有个人信息之后，知道你是谁了，才放行
      // 每一次路由跳转时都会触发接口请求，有了用户信息之后，就不需要再请求了
      if (!store.state.user.userInfo.userId) {
        const { roles } = await store.dispatch('user/getuserInfo')
        const otherRoutes = await store.dispatch(
          'routes/filterRoutes',
          roles.menus
        )
        console.log(otherRoutes)

        // router.options.routes 只会拿到初始化时的 routes规则，后续动态新增的路由规则，拿不到
        router.addRoutes([
          ...otherRoutes,
          { path: '*', redirect: '/404', hidden: true }
        ])
        next({
          ...to, // next({ ...to })的目的,是保证路由添加完了再进入页面 (可以理解为重进一次)
          replace: true // 重进一次, 不保留重复历史
        })
      }
      // 等一个异步的操作结束之后，再做某件事
      next()
    }
  } else {
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
      NProgress.done()
    }
  }
})
// 路由后置守卫(在页面进入之后，会经过后置守卫)

// 全局后置守卫，有一个注意点：一旦在全局前置中做了拦截，拦截到了其他的页面，后置守卫不会被触发
// 解决方案：如果在前置守卫做了拦截，就需要手动关闭NProgress
router.afterEach(() => {
  // 关闭进度条
  NProgress.done()
})

// import router from './router'
// import store from './store'
// import { Message } from 'element-ui'
// import NProgress from 'nprogress' // 进度条小插件
// import 'nprogress/nprogress.css' // progress bar style
// import { getToken } from '@/utils/auth' // get token from cookie
// import getPageTitle from '@/utils/get-page-title'

// NProgress.configure({ showSpinner: false }) // NProgress Configuration

// // 白名单
// const whiteList = ['/login'] // no redirect whitelist

// // 路由守卫
// // 路由前置守卫：所有的路由在真正被访问到之前，都会先经过路由前置守卫。
// // 只有前置守卫放行了，才能继续进入到所访问到的页面。
// router.beforeEach(async (to, from, next) => {
//   // start progress bar
//   // 开启进度条
//   NProgress.start()

//   // set page title
//   document.title = getPageTitle(to.meta.title)

//   // determine whether the user has logged in
//   // 获取到用户的token
//   const hasToken = getToken()

//   if (hasToken) {
//     // 有token，说明当前用户已经登陆了
//     if (to.path === '/login') {
//       // if is logged in, redirect to the home page
//       next({ path: '/' })
//       // 关闭进度条
//       NProgress.done()
//     } else {
//       // 用户的名字
//       const hasGetUserInfo = store.getters.name
//       if (hasGetUserInfo) {
//         next()
//       } else {
//         try {
//           // get user info
//           await store.dispatch('user/getInfo')

//           next()
//         } catch (error) {
//           // remove token and go to login page to re-login
//           await store.dispatch('user/resetToken')
//           Message.error(error || 'Has Error')
//           next(`/login?redirect=${to.path}`)
//           NProgress.done()
//         }
//       }
//     }
//   } else {
//     /* has no token*/
//     // 没有token，说明当前用户没登录

//     // 黑马头条：
//     // 登录了：个人信息页面，发评论
//     // 未登录：频道列表、文章列表、视频、。。。。

//     // 人资
//     // 登录了：正常没啥问题
//     // 未登录：只能打开白名单页面
//     if (whiteList.indexOf(to.path) !== -1) {
//       // in the free login whitelist, go directly
//       next()
//     } else {
//       // other pages that do not have permission to access are redirected to the login page.
//       next(`/login?redirect=${to.path}`)
//       NProgress.done()
//     }
//   }
// })

// // 路由后置守卫
// router.afterEach(() => {
//   // finish progress bar
//   // 关闭进度条
//   NProgress.done()
// })
