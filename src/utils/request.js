import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
// import { getToken } from '@/utils/auth'

// create an axios instance
const request = axios.create({
  baseURL: process.env.VUE_APP_BASE_API // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  // timeout: 5000 // request timeout
})

// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    const token = store.getters.token
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    // 必须要return ，否则请求到不了服务器
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
request.interceptors.response.use(
  // 状态吗：200   201 、、、
  function (response) {
    const { message, success } = response.data

    // 希望对所有的响应 success: false 的情况，统一处理，抛出错误
    // 解决Bug2：在响应拦截器对接口返回的success做判断，success为false时，表示接口请求出错
    if (!success) {
      // 统一给个错误的提示
      Message.error(message)
      // return new Promise((resolve, reject) => {
      //   reject()
      // }
      return Promise.reject(new Error(message))
    }
    // 对响应数据做点什么
    // 请求接口的地方减少一层data
    return response.data
  },
  // 处理 400 401 404 500 502..
  function (error) {
    // 对响应错误做点什么
    if (error.response.status === 401 && error.response.data.code === 10002) {
      // 登录过期的处理
      Message.error('亲！您的登录状态已过期，请重新登录！')
      // token过期了，同样清除用户个人资料
      store.dispatch('user/logout')
      router.push('/login')
    } else {
      // 其他请求错误的处理
      Message.error(error.message)
    }
    return Promise.reject(error)
  }
)
export default request
