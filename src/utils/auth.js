// js-cookie 好东西，非常好用，语法和localStorage非常的相近 （cookie也只能存字符串）
// 1. 获取 Cookies.get(name)          类似  localStorage.getItem(name)
// 2. 设置 Cookies.set(name, value)   类似  localStorage.setItem(name, value)
// 3. 移除 Cookies.remove(name)       类似  localStorage.removeItem(name, value)

import Cookies from 'js-cookie'

const TokenKey = 'userToken'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
