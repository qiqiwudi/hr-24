export default {
  // data 中声明的数据
  // 1、也会混入给组件，作为数据使用
  // 2、组件内如果和mixin的data中的变量重名了以组件内为准

  // methods 同 data

  // 生命周期，如果配置相同的生命周期函数
  // 1、不会覆盖
  // 2、会合并，依次执行。 （mixin的生命周期先、组件生命周期后）
  data() {
    return {
      title: 'mixin的title'
    }
  },
  created() {
    // console.log('mixin:' + this.title)
  },
  methods: {
    // lisi() {
    //   console.log('我是mixin里的lisi方法')
    // }
  }
}
