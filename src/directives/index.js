// import Vue from 'vue'

// 1. 不能所有的指令都全局定义，这样限制死了前端同事的使用方式
//    应该的方式：前端同事想全局就全局，想局部就局部
// 2. 自定义指令模块，不止一个指令，有很多个指令

export const imgerror = {
  inserted(el, binding) {
    el.onerror = function () {
      el.src = binding.value
    }
  }
}

export const textcolor = {
  inserted(el, binding) {
    el.style.color = binding.value
  }
}

// 注册全局的自定义指令
// 统一处理图片加载失败的情况
// Vue.directive('imgerror', {
//   inserted(el, binding) {
//     // el 指令所在的元素
//     // binding: 指令相关的信息对象， binding.value 指令的值
//     // onerror(原生的方法)可以捕获到图片加载失败
//     el.onerror = function () {
//       el.src = binding.value
//     }
//   }
// })

// 修改文字颜色
// Vue.directive('textcolor', {
//     inserted(el, binding) {
//       el.style.color = binding.value
//     }
//   })
