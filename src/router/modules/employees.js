import Layout from '@/layout'
export default {
  path: '/employees',
  component: Layout,
  children: [
    // 组织架构的默认首页
    {
      path: '',
      name: 'employees',
      component: () => import('@/views/employees/index.vue'),
      meta: { title: '员工', icon: 'people' }
    },
    {
      path: 'detail/:id',
      name: 'employeesdetail',
      component: () => import('@/views/employees/detail.vue'),
      // meta: { title: '员工详情', icon: 'people' },
      hidden: true
    },
    // 员工打印页
    {
      path: 'print/:id',
      component: () => import('@/views/employees/print'),
      hidden: true,
      meta: {
        title: '员工打印'
      }
    }
  ]
}
