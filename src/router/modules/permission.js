import Layout from '@/layout'
export default {
  path: '/permission',
  component: Layout,
  children: [
    // 权限管理的默认首页
    {
      path: '',
      name: 'permissions',
      component: () => import('@/views/permission/index.vue'),
      meta: { title: '权限管理', icon: 'lock' }
    }
  ]
}
