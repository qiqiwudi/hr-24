import Layout from '@/layout'
export default {
  path: '/social',
  component: Layout,
  children: [
    // 社保的默认首页
    {
      path: '',
      name: 'social_securitys',
      component: () => import('@/views/social/index.vue'),
      meta: { title: '社保', icon: 'table' }
    }
  ]
}
