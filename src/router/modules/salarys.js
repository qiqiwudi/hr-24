import Layout from '@/layout'
export default {
  path: '/salarys',
  component: Layout,
  children: [
    // 工资的默认首页
    {
      path: '',
      name: 'salarys',
      component: () => import('@/views/salarys/index.vue'),
      meta: { title: '工资', icon: 'money' }
    }
  ]
}
