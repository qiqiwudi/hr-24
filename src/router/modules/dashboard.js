import Layout from '@/layout'
export default {
  path: '/',
  component: Layout,
  redirect: '/dashboard',
  children: [
    // 首页
    {
      path: 'dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }
  ]
}
