import Layout from '@/layout'
export default {
  path: '/setting',
  component: Layout,
  children: [
    // 公司设置的默认首页
    {
      path: '',
      name: 'settings',
      component: () => import('@/views/setting/index.vue'),
      meta: { title: '公司设置', icon: 'setting' }
    }
  ]
}
