import Layout from '@/layout'
export default {
  path: '/attendances',
  component: Layout,
  children: [
    // 考勤的默认首页
    {
      path: '',
      name: 'attendances',
      component: () => import('@/views/attendances/index.vue'),
      meta: { title: '考勤', icon: 'skill' }
    }
  ]
}
