import Layout from '@/layout'
export default {
  path: '/departments',
  component: Layout,
  children: [
    // 组织架构的默认首页
    {
      path: '',
      name: 'departments',
      component: () => import('@/views/departments/index.vue'),
      meta: { title: '组织架构', icon: 'tree' }
    }
  ]
}
