import Layout from '@/layout'
export default {
  path: '/approvals',
  component: Layout,
  children: [
    // 审批的默认首页
    {
      path: '',
      name: 'approvals',
      component: () => import('@/views/approvals/index.vue'),
      meta: { title: '审批', icon: 'tree-table' }
    }
  ]
}
