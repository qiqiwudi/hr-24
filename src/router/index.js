import Vue from 'vue'
import Router from 'vue-router'

// 首页
import dashboardRouter from './modules/dashboard'

// 下面的是动态路由
import approvalsRouter from './modules/approvals'
import attendancesRouter from './modules/attendances'
import departmentsRouter from './modules/departments'
import employeesRouter from './modules/employees'
import permissionRouter from './modules/permission'
import salarysRouter from './modules/salarys'
import settingRouter from './modules/setting'
import socialRouter from './modules/social'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

// 路由和组件的映射关系数组
// 动态路由表 => 动态路由(需要权限才可以访问的) 我们这里准备一个数组存放
export const asyncRoutes = [
  departmentsRouter,
  settingRouter,
  employeesRouter,
  permissionRouter,
  approvalsRouter,
  attendancesRouter,
  salarysRouter,
  socialRouter
]

// 静态路由表：包含了所有的静态路由
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  // {
  //   path: '/import',
  //   component: () => import('@/views/import'),
  //   hidden: true
  // },
  {
    path: '/import',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '',
        name: 'import',
        component: () => import('@/views/import'),
        meta: { title: '文件导入' }
      }
    ]
  },
  dashboardRouter
  // { path: '*', redirect: '/404', hidden: true }
]

// 每个角色看到的左侧菜单是不一样的
// 张三（管理员）：能看到所有的页面
// 李四（普通员工）：只能看3个页面
// 王五（小主管）：能看到6个页面
// const 动态路由 = [
//   {
//     path: 路由路径,
//     component: 组件路径
//   }
// ]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    // routes: [...constantRoutes, ...asyncRoutes]
    // routes里面的数据不是响应性的
    routes: [...constantRoutes]
    // routes: [...constantRoutes, ...动态路由]
  })

const router = createRouter()

// 给routes添加新的路由规则
// router.addRoutes([...asyncRoutes])

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// 重置路由
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
