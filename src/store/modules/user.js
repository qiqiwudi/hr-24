import { login, getUserinfoApi, getSysUserInfoApi } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const state = {
  token: getToken() || '', // 用户的token
  userInfo: {} // 用户资料
}
const mutations = {
  // 存储token
  setToken(state, newToken) {
    state.token = newToken
  },
  // 存储用户资料
  setUserInfo(state, newUserInfo) {
    state.userInfo = newUserInfo
  },
  // 清除token
  removeToken(state) {
    removeToken()
    state.token = ''
  },
  // 清除用户资料
  removeUserInfo(state) {
    state.userInfo = {}
  }
}
const actions = {
  // 走登录接口
  async getlogin({ commit }, data) {
    // 1. 发送请求，获取 token
    const res = await login(data)
    console.log(res)
    // 2. 提交mutation，将token存到state中
    commit('setToken', res.data)
    // 3.对token做长期保存
    setToken(res.data)
    return res
  },
  // getlogin({ commit }, data) {
  //   // 要把接口请求失败或成功的结果返回给getlogin
  //   const p = new Promise((resolve, reject) => {
  //     login(data)
  //       .then(() => {
  //         resolve()
  //       })
  //       .catch((err) => {
  //         console.log(err)
  //         reject()
  //       })
  //   })
  //   return p
  // }

  // 请求接口拿用户的基本资料
  async getuserInfo({ commit }) {
    // 1、这个接口不带用户头像
    const { data: data1 } = await getUserinfoApi()
    // 2、基于前面的得到的结果（userId）来请求
    const { data: data2 } = await getSysUserInfoApi(data1.userId)
    // 3、合并用户的个人信息数据
    const obj = {
      ...data1,
      ...data2
    }
    commit('setUserInfo', obj)
    return obj
    // const p = new Promise((resolve, reject) => {
    //   getUserinfoApi()
    //     .then((res) => {
    //       resolve()
    //       commit('setUserInfo', res.data)
    //     })
    //     .catch(() => {
    //       reject()
    //     })
    // })
    // return p
  },

  // 退出登录
  logout({ commit }) {
    commit('removeToken')
    commit('removeUserInfo')
    resetRouter()
    commit('routes/setRoutes', [], { root: true })
    commit('settings/resetTheme', [], { root: true })
  }
}
const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
