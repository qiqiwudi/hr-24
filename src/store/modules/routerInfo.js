import { constantRoutes, asyncRoutes } from '@/router'
const state = {
  router: constantRoutes || []
}

const mutations = {
  // otherRoutes登录成功后, 需要添加的新路由(根据用户权限筛选出来的动态路由)
  setRoutes(state, otherRoutes) {
    // 静态路由基础上, 累加其他权限路由
    state.router = [...constantRoutes, ...otherRoutes]
  }
}

const actions = {
  // 筛选路由权限
  // menus根据接口返回的用户具备的权限点
  filterRoutes(context, menus) {
    const otherRoutes = asyncRoutes.filter((item) =>
      menus.includes(item.children[0].name)
    )
    context.commit('setRoutes', otherRoutes)
    console.log(otherRoutes)
    return otherRoutes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
