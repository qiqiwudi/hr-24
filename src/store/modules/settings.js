import defaultSettings from '@/settings'
import Cookies from 'js-cookie'
const { showSettings, fixedHeader, sidebarLogo } = defaultSettings
const themeKey = 'hm-hrsaas-theme-color'
const themeDefaultColor = '#1890FF'
const state = {
  showSettings: showSettings,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo,
  theme: Cookies.get(themeKey) || themeDefaultColor // 存储所选择的颜色
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
      // 对象.键名 = 值
      // 对象[键名] = 值
    }
    if (key === 'theme') {
      Cookies.set(themeKey, value)
    }
  },

  // 退出登录清除主题色
  resetTheme(state) {
    // 重置成默认颜色
    state.theme = themeDefaultColor
    // cookies中也要移出
    Cookies.remove(themeKey)
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
