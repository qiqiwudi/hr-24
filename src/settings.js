module.exports = {
  // 网站的标题
  title: '人力资源后台管理系统',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  // 控制页面的头部是否跟随滚动
  fixedHeader: true,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // 左侧菜单顶部是否显示logo
  sidebarLogo: true
}
