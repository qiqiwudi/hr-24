import request from '@/utils/request'

/**
 * 获取-员工列表
 * @returns 返回一个promise
 */
export function getUserListApi(page = 1, size = 5) {
  return request({
    url: '/sys/user',
    method: 'get',
    params: {
      page,
      size
    }
  })
}

/**
 * 新增员工
 * @returns 返回一个promise
 */
export function addUserApi(data) {
  return request({
    url: '/sys/user',
    method: 'post',
    data
  })
}

/**
 * 批量新增员工
 * @returns 返回一个promise
 */
export function addUserBatchApi(arr) {
  return request({
    url: '/sys/user/batch',
    method: 'post',
    data: arr
  })
}

/** *
 * 更新员工的基本信息
 * **/
export function reqSaveUserDetailById(data) {
  return request({
    method: 'put',
    url: `/sys/user/${data.id}`,
    data
  })
}

/** *
 *  获取员工-个人信息
 * **/
export function reqGetPersonalDetail(id) {
  return request({
    method: 'get',
    url: `/employees/${id}/personalInfo`
  })
}

/** *
 *  更新员工-个人信息（这个接口，文档里没有，需注意，可在更多接口文档中查看）
 * **/
export function reqUpdatePersonal(data) {
  return request({
    method: 'put',
    url: `/employees/${data.userId}/personalInfo`,
    data
  })
}

/** **
 * 获取用户的岗位信息  (岗位信息)
 * ****/
export function reqGetJobDetail(id) {
  return request({
    method: 'get',
    url: `/employees/${id}/jobs`
  })
}

/**
 * 更新岗位信息  (岗位信息)（这个接口，文档里没有，需注意，可在更多接口文档中查看）
 * ****/
export function reqUpdateJob(data) {
  return request({
    method: 'put',
    url: `/employees/${data.userId}/jobs`,
    data
  })
}

// 分配-员工-角色
/**
 *
 * @param {String} id 员工id
 * @param {Array} roleIds 角色id数组
 * @returns 返回promise
 */
export function assignRolesApi(id, roleIds) {
  return request({
    method: 'put',
    url: '/sys/user/assignRoles',
    data: {
      id,
      roleIds
    }
  })
}
