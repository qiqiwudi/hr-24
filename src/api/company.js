// 获取公司详情
import request from '@/utils/request'

// 根据 id 查询企业
export const getCompanyApi = (id) => {
  return request({
    method: 'get',
    url: `/company/${id}`
  })
}
