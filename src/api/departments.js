import request from '@/utils/request'

/**
 * 获取-部门列表
 * @returns 返回一个promise
 */
export function getDepartmentListApi() {
  return request({
    url: '/company/department',
    method: 'get'
  })
}

/**
 * 获取-部门负责人列表
 * @returns 返回一个promise
 */
export function getLeaderListApi() {
  return request({
    url: '/sys/user/simple',
    method: 'get'
  })
}

/**
 * 新增部门
 * @returns 返回一个promise
 */
export function addDepartmentApi(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}

// 删除-部门
export function delDepartmentApi(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
  })
}

// 获取-部门详情
export function getDepartmentApi(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'get'
  })
}

// 编辑-部门详情
export function editDepartmentApi(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}
