import request from '@/utils/request'

/**
 * 获取-角色列表
 * @returns 返回一个promise
 */
export function getRoleListApi(page, pagesize = 10) {
  return request({
    url: '/sys/role',
    method: 'get',
    params: {
      page,
      pagesize
    }
  })
}

/**
 * 新增-角色
 * @returns 返回一个promise
 */
export function addRoleApi(data) {
  return request({
    url: '/sys/role',
    method: 'post',
    data
  })
}

/**
 * 删除-角色
 * @returns 返回一个promise
 */
export function delRoleApi(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'delete'
  })
}

/**
 * 获取角色详情
 * @returns 返回一个promise
 */
export function getRoleDetailApi(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'get'
  })
}

/**
 * 编辑角色详情
 * @returns 返回一个promise
 */
export function editRoleDetailApi(data) {
  return request({
    url: `/sys/role/${data.id}`,
    method: 'put',
    data
  })
}

/**
 * 给角色分配权限
 * url /sys/role/assignPrem
 */

export function assignPremApi(data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}
