import request from '@/utils/request'

/**
 * 登录接口封装
 * @param {Object} data 传递的手机号和密码
 * @returns 返回一个promise
 */
export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

/**
 * 获取-用户基本资料
 * @returns 返回一个promise
 */
export function getUserinfoApi() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

/**
 * 获取-员工-基本信息
 * @returns 返回一个promise
 */
export function getSysUserInfoApi(id) {
  return request({
    url: `/sys/user/${id}`,
    method: 'get'
  })
}

// 获取员工-个人信息
export function getStaffInfoApi(id) {
  return request({
    url: `/sys/user/${id}`,
    method: 'get'
  })
}

// 获取部门负责人列表
export function getSimpleListApi() {
  return request({
    url: '/sys/user/simple',
    method: 'get'
  })
}
