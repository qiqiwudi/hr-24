// 这里定义 index.js 专门用于快速全局注册组件
// 将来 main 中 Vue.use 一下即可快速注册所有全局组件
import PageTools from '@/components/PageTools'
import UploadExcel from '@/components/UploadExcel'
import ImageUpload from '@/components/ImageUpload'
import ScreenFull from '@/components/ScreenFull'
import ThemePicker from '@/components/ThemePicker'
import Lang from '@/components/Lang'

export default {
  // 定义插件， 将来 Vue.use 使用
  // 插件是一个对象，必须提供 install 方法
  // 1. 在 Vue.use 后，会自动调用插件的 install 方法
  // 2. install 方法中，可以提供组件全局注册的代码
  install(Vue) {
    Vue.component('PageTools', PageTools)
    Vue.component('UploadExcel', UploadExcel)
    Vue.component('ImageUpload', ImageUpload)
    Vue.component('ScreenFull', ScreenFull)
    Vue.component('ThemePicker', ThemePicker)
    Vue.component('Lang', Lang)
  }
}
